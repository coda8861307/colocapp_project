import viewHeader from './header';
import viewFooter from './footer';

export default () => (
  `
  ${viewHeader()}
  <main class="main-content">
    <section class="register-section">
        <div class="register-form">
            <h2>Inscription</h2>
            <form action="/register" method="post" id="register-form">
            <div class="form-group">
                <label for="username">Nom d'utilisateur</label>
                <input type="text" id="username" name="username" required>
            </div>
            <div class="form-group">
                <label for="first-name">Prénom</label>
                <input type="text" id="first-name" name="firstName" required>
            </div>
            <div class="form-group">
                <label for="last-name">Nom de famille</label>
                <input type="text" id="last-name" name="lastName" required>
            </div>
            <div class="form-group">
                <label for="email">Adresse e-mail</label>
                <input type="email" id="email" name="email" required>
            </div>
            <div class="form-group">
                <label for="password">Mot de passe</label>
                <input type="password" id="password" name="password" required>
            </div>
            <div class="form-group">
                <label for="confirm-password">Confirmer le mot de passe</label>
                <input type="password" id="confirm-password" name="confirm-password" required>
            </div>
            <button type="submit">S'inscrire</button>
            </form>
            <p>Déjà un compte ? <a href="/login">Connectez-vous</a></p>
        </div>
    </section>
  </main>
  ${viewFooter()}
`
);
