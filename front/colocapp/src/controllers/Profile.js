import viewProfile from '../views/profile';
import { toggleNotificationMenu, toggleHamburgerMenu, closeHamburgerMenu } from '../components/menufunction';

const Profile = class {
  constructor() {
    this.el = document.querySelector('#root');
    this.run();
    this.setupEventListeners();
  }

  setupEventListeners() {
    // Bouton de notification
    const notificationButton = document.querySelector('.header-notif-button');
    if (notificationButton) {
      notificationButton.addEventListener('click', toggleNotificationMenu);
    }

    // Fermer le menu de notification si on clique en dehors
    document.addEventListener('click', (event) => {
      const notificationMenu = document.querySelector('.notification-menu');
      const isClickInside = notificationMenu.contains(event.target) || event.target.closest('.header-notif-button');

      if (!isClickInside && notificationMenu) {
        notificationMenu.style.display = 'none';
      }
    });

    // Bouton du menu hamburger
    const hamburgerButton = document.querySelector('.header-hamburger-button');
    if (hamburgerButton) {
      hamburgerButton.addEventListener('click', toggleHamburgerMenu);
    }

    // Fermer le menu hamburger si on clique en dehors
    document.addEventListener('click', (event) => {
      const hamburgerMenu = document.querySelector('.header-hamburger-container');
      const isClickInside = hamburgerMenu.contains(event.target) || event.target.closest('.header-hamburger-button');

      if (!isClickInside && hamburgerMenu.classList.contains('show')) {
        closeHamburgerMenu();
      }
    });

    // Bouton de fermeture dans le menu hamburger
    const closeHamburgerButton = document.querySelector('.header-hamburger-close');
    if (closeHamburgerButton) {
      closeHamburgerButton.addEventListener('click', closeHamburgerMenu);
    }
  }

  render() {
    return `
    ${viewProfile()}
    `;
  }

  run() {
    this.el.innerHTML = this.render();
  }
};

export default Profile;
