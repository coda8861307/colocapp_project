<?php

require __DIR__ . '/../vendor/autoload.php';

use Back\Src\Router;
use Back\Src\Controllers\UserController;

header('Access-Control-Allow-Origin: http://127.0.0.1:9090');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, Authorization');
header('Access-Control-Allow-Credentials: true');

// Vérifie si la méthode est OPTIONS et envoie une réponse appropriée
if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    http_response_code(200);
    exit();
}

// Définir les routes
$routes = [
    '/register' => [UserController::class, 'postRegister'],
    '/login' => [UserController::class, 'postLogin'],
];

// Créer et exécuter le routeur
new Router($routes);
