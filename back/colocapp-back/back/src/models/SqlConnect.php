<?php

namespace Back\Src\Models;

use PDO;

// Headers CORS
header("Access-Control-Allow-Origin: http://127.0.0.1:9090");
header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
header("Access-Control-Allow-Credentials: true");

// Si la méthode est OPTIONS, renvoyer une réponse 200
if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    http_response_code(200);
    exit();
}

class SqlConnect {
    protected PDO $db;

    public function __construct() {
        $config = require __DIR__ . '/../config/config.php';
        $dbConfig = $config['db'];

        $this->db = new PDO(
            'mysql:host=' . $dbConfig['host'] . ';port=' . $dbConfig['port'] . ';dbname=' . $dbConfig['dbname'],
            $dbConfig['user'],
            $dbConfig['password']
        );

        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->db->setAttribute(PDO::ATTR_PERSISTENT, false);
    }

    public function transformDataInDot(array $data): array {
        $dataFormatted = [];
        foreach ($data as $key => $value) {
            $dataFormatted[':' . $key] = $value;
        }
        return $dataFormatted;
    }
}
