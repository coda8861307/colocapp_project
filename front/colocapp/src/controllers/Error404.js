const Error404 = class {
  constructor() {
    this.el = document.querySelector('#root');
    this.run();
  }

  render() {
    return `
    <div class="error-container">
        <h1 class="error-title">404</h1>
        <p class="error-message">Désolé, la page que vous cherchez n'existe pas.</p>
        <a class="error-link" href="/calendar">Retour à la page d'accueil</a>
    </div>
      `;
  }

  run() {
    this.el.innerHTML = this.render();
  }
};

export default Error404;
