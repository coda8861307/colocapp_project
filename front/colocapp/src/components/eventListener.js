import {
  toggleNotificationMenu,
  closeNotificationMenu,
  toggleHamburgerMenu,
  closeHamburgerMenu
} from './headerFunction';

const setupHeaderEventListeners = () => {
  // Bouton de notification
  const notificationButton = document.querySelector('.header-notif-button');
  if (notificationButton) {
    notificationButton.addEventListener('click', toggleNotificationMenu);
  }

  // Fermer le menu de notification si on clique en dehors
  document.addEventListener('click', (event) => {
    const notificationMenu = document.querySelector('.notification-menu');
    const isClickInside = notificationMenu && (notificationMenu.contains(event.target) || event.target.closest('.header-notif-button'));

    if (!isClickInside && notificationMenu) {
      closeNotificationMenu();
    }
  });

  // Bouton du menu hamburger
  const hamburgerButton = document.querySelector('.header-hamburger-button');
  if (hamburgerButton) {
    hamburgerButton.addEventListener('click', toggleHamburgerMenu);
  }

  // Fermer le menu hamburger si on clique en dehors
  document.addEventListener('click', (event) => {
    const hamburgerMenu = document.querySelector('.header-hamburger-container');
    const isClickInside = hamburgerMenu && (hamburgerMenu.contains(event.target) || event.target.closest('.header-hamburger-button'));

    if (!isClickInside && hamburgerMenu && hamburgerMenu.classList.contains('show')) {
      closeHamburgerMenu();
    }
  });

  // Bouton de fermeture dans le menu hamburger
  const closeHamburgerButton = document.querySelector('.header-hamburger-close');
  if (closeHamburgerButton) {
    closeHamburgerButton.addEventListener('click', closeHamburgerMenu);
  }
};

export default setupHeaderEventListeners;
