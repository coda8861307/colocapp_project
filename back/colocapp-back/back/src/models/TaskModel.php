<?php

namespace Back\Src\Models;

use PDO;

class TaskModel extends SqlConnect {
    public function getAllTasks() {
        $stmt = $this->db->prepare("SELECT * FROM tasks");
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getTaskById($id) {
        $stmt = $this->db->prepare("SELECT * FROM tasks WHERE id = :id");
        $stmt->execute([':id' => $id]);
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function createTask($name, $description, $date, $author) {
        $stmt = $this->db->prepare("INSERT INTO tasks (name, description, date, author) VALUES (:name, :description, :date, :author)");
        $stmt->execute([
            ':name' => $name,
            ':description' => $description,
            ':date' => $date,
            ':author' => $author
        ]);
        return $this->db->lastInsertId();
    }

    public function updateTask($id, $name, $description, $date, $author, $completed) {
        $stmt = $this->db->prepare("UPDATE tasks SET name = :name, description = :description, date = :date, author = :author, completed = :completed WHERE id = :id");
        return $stmt->execute([
            ':name' => $name,
            ':description' => $description,
            ':date' => $date,
            ':author' => $author,
            ':completed' => $completed,
            ':id' => $id
        ]);
    }

    public function deleteTask($id) {
        $stmt = $this->db->prepare("DELETE FROM tasks WHERE id = :id");
        return $stmt->execute([':id' => $id]);
    }
}
