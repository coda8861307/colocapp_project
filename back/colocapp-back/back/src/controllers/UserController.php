<?php

namespace Back\Src\Controllers;

use Back\Src\Models\UserModel;

// Headers CORS
header("Access-Control-Allow-Origin: http://127.0.0.1:9090");
header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
header("Access-Control-Allow-Credentials: true");

// Si la méthode est OPTIONS, renvoyer une réponse 200
if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    http_response_code(200);
    exit();
}

class UserController extends Controller {
    private $userModel;

    public function __construct($params) {
        $this->userModel = new UserModel();
        parent::__construct($params);
    }

    protected function postRegister() {
        if (!$this->body || empty($this->body['username']) || empty($this->body['firstName']) || 
            empty($this->body['lastName']) || empty($this->body['email']) || empty($this->body['password'])) {
            http_response_code(400); // Bad Request
            return ['status' => false, 'message' => 'Veuillez remplir tous les champs'];
        }

        $result = $this->userModel->add((array) $this->body);
        http_response_code($result['status'] ? 201 : 400);
        return $result;
    }

    protected function postLogin() {
        if (!$this->body || empty($this->body['username']) || empty($this->body['password'])) {
            http_response_code(400); // Bad Request
            return ['status' => false, 'message' => 'Veuillez entrer un nom d\'utilisateur et un mot de passe'];
        }

        $result = $this->userModel->login($this->body['username'], $this->body['password']);
        http_response_code($result['status'] ? 200 : 401);
        return $result;
    }
}
