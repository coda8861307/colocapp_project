import viewHeader from './header';
import viewFooter from './footer';

export default () => (
  `
  ${viewHeader()}
  <main class="main-content">
    <section class="section">
        <div class="profile-main">
            <div class="profile-container">
                <div class="profile-header">
                <div class="profile-photo">
                    <img src="default-avatar.png" alt="Photo de Profil" id="profile-photo-img">
                    <input type="file" id="profile-photo-input" accept=".jpg,.jpeg,.png" style="display: none;">
                </div>
                <div class="profile-info">
                    <h1 id="profile-full-name">Jean Dupont</h1>
                    <p id="profile-username">@jdupont</p>
                    <p id="profile-email">jean.dupont@example.com</p>
                </div>
                <button id="edit-profile-btn" class="edit-profile-btn">
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24">
                    <path fill="currentColor" d="M21.731 2.269a2.625 2.625 0 0 0-3.712 0l-1.157 1.157l3.712 3.712l1.157-1.157a2.625 2.625 0 0 0 0-3.712Zm-2.218 5.93l-3.712-3.712l-12.15 12.15a5.25 5.25 0 0 0-1.32 2.214l-.8 2.685a.75.75 0 0 0 .933.933l2.685-.8a5.25 5.25 0 0 0 2.214-1.32L19.513 8.2Z" />
                    </svg>
                </button>
                </div>
                <div class="profile-details">
                <div class="profile-section">
                    <h2>Informations Personnelles</h2>
                    <p><strong>Nom:</strong> <span id="profile-last-name">Dupont</span></p>
                    <p><strong>Prénom:</strong> <span id="profile-first-name">Jean</span></p>
                    <p><strong>Nom d'utilisateur:</strong> <span id="profile-username-detail">@jdupont</span></p>
                    <p><strong>Email:</strong> <span id="profile-email-detail">jean.dupont@example.com</span></p>
                </div>
                </div>
            </div>
        </div>
    
        <div id="custom-profile-popup" class="custom-popup">
            <div class="custom-popup-content">
                <span class="custom-close-popup">&times;</span>
                <h2>Modifier le Profil</h2>
                <form id="custom-profile-form">
                <div class="custom-form-group">
                    <label for="profile-last-name-input">Nom :</label>
                    <input type="text" id="profile-last-name-input" name="last_name" required>
                </div>
                <div class="custom-form-group">
                    <label for="profile-first-name-input">Prénom :</label>
                    <input type="text" id="profile-first-name-input" name="first_name" required>
                </div>
                <div class="custom-form-group">
                    <label for="profile-username-input">Nom d'utilisateur :</label>
                    <input type="text" id="profile-username-input" name="username" required>
                </div>
                <div class="custom-form-group">
                    <label for="profile-email-input">Email :</label>
                    <input type="email" id="profile-email-input" name="email" required>
                </div>
                <div class="custom-form-group">
                    <label for="profile-password-input">Mot de Passe :</label>
                    <input type="password" id="profile-password-input" name="password" required>
                </div>
                <div class="custom-form-group">
                    <label for="profile-password-confirm-input">Confirmez le Mot de Passe :</label>
                    <input type="password" id="profile-password-confirm-input" name="password_confirm" required>
                </div>
                <button type="submit" class="custom-save-profile">Enregistrer</button>
                </form>
            </div>
        </div>
    </section>
  </main>
  ${viewFooter()}
  `
);
