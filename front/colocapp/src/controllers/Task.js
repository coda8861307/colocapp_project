import axios from 'axios';
import Swal from 'sweetalert2';
import setupHeaderEventListeners from '../components/eventListener';
import viewTasks from '../views/tasks';
// import viewTask from '../views/task';

const Task = class {
  constructor(params) {
    this.params = params;
    this.el = document.querySelector('#root');
    this.baseUrl = 'http://localhost:83';
    this.tasks = [];
    this.run();
  }

  async fetchTasks() {
    try {
      const response = await axios.get(`${this.baseUrl}/tasks`);
      this.tasks = response.data;
      this.render();
    } catch (error) {
      console.error('Error fetching tasks:', error);
      Swal.fire({
        icon: 'error',
        title: 'Erreur',
        text: 'Erreur lors de la récupération des tâches.'
      });
      this.render();
    }
  }

  async createTask(data) {
    try {
      const response = await axios.post(`${this.baseUrl}/tasks`, data);
      this.tasks.push(response.data);
      this.render();
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Tâche créée avec succès!',
        showConfirmButton: false,
        timer: 1500
      });
    } catch (error) {
      Swal.fire({
        icon: 'error',
        title: 'Erreur',
        text: 'Erreur lors de la création de la tâche.'
      });
    }
  }

  async updateTask(id, data) {
    try {
      const response = await axios.put(`${this.baseUrl}/tasks/${id}`, data);
      const index = this.tasks.findIndex((task) => task.id === id);
      this.tasks[index] = response.data;
      this.render();
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Tâche mise à jour avec succès!',
        showConfirmButton: false,
        timer: 1500
      });
    } catch (error) {
      Swal.fire({
        icon: 'error',
        title: 'Erreur',
        text: 'Erreur lors de la mise à jour de la tâche.'
      });
    }
  }

  async deleteTask(id) {
    try {
      await axios.delete(`${this.baseUrl}/tasks/${id}`);
      this.tasks = this.tasks.filter((task) => task.id !== id);
      this.render();
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Tâche supprimée avec succès!',
        showConfirmButton: false,
        timer: 1500
      });
    } catch (error) {
      Swal.fire({
        icon: 'error',
        title: 'Erreur',
        text: 'Erreur lors de la suppression de la tâche.'
      });
    }
  }

  handleTaskSubmit(event) {
    event.preventDefault();

    const nameElement = document.getElementById('custom-task-name');
    const descriptionElement = document.getElementById('custom-task-description');
    const dateElement = document.getElementById('custom-task-date');
    const authorElement = document.getElementById('custom-task-author');
    const idElement = document.getElementById('task-id'); // Hidden input to store task id for editing

    // Vérifiez que tous les éléments du formulaire existent
    if (!nameElement || !descriptionElement || !dateElement || !authorElement || !idElement) {
      console.error('Some form elements are missing.');
      return;
    }

    const name = nameElement.value;
    const description = descriptionElement.value;
    const date = dateElement.value;
    const author = authorElement.value;
    const id = idElement.value;

    if (!name || !description || !date || !author) {
      Swal.fire({
        icon: 'error',
        title: 'Erreur',
        text: 'Veuillez compléter tous les champs.'
      });
      return;
    }

    const data = {
      name,
      description,
      date,
      author
    };

    if (id) {
      this.updateTask(id, data);
    } else {
      this.createTask(data);
    }

    this.closePopup();
  }

  handleTaskDelete(event) {
    const taskId = event.target.closest('.task-info').dataset.id;
    this.deleteTask(taskId);
  }

  handleTaskMove(event) {
    const taskElement = event.target.closest('.task-info');
    const taskId = taskElement.dataset.id;
    const task = this.tasks.find((t) => t.id === taskId);
    task.completed = !task.completed;
    this.updateTask(taskId, task);
  }

  openPopup(task) {
    const taskPopup = document.getElementById('custom-task-popup');
    const titleElement = document.querySelector('.custom-task-popup-content h2');
    const nameInput = document.getElementById('custom-task-name');
    const descriptionInput = document.getElementById('custom-task-description');
    const dateInput = document.getElementById('custom-task-date');
    const authorInput = document.getElementById('custom-task-author');
    const idInput = document.getElementById('task-id'); // Assuming you have this hidden input

    try {
      if (!taskPopup
        || !titleElement
        || !nameInput
        || !descriptionInput
        || !dateInput
        || !authorInput) {
        throw new Error('One or more elements not found.');
      }

      if (task) {
        titleElement.innerText = 'Modifier la Tâche';
        nameInput.value = task.name;
        descriptionInput.value = task.description;
        dateInput.value = task.date;
        authorInput.value = task.author;
        if (idInput) {
          idInput.value = task.id; // Hidden input to store task id for editing
        }
      } else {
        titleElement.innerText = 'Créer une Nouvelle Tâche';
        document.getElementById('custom-task-form').reset();
      }

      taskPopup.style.display = 'block';
      document.body.classList.add('blurred', 'no-scroll');
    } catch (error) {
      console.error('Error opening popup:', error);
      Swal.fire({
        icon: 'error',
        title: 'Erreur',
        text: 'Une erreur est survenue lors de l\'ouverture de la popup.'
      });
    }
  }

  closePopup() {
    const taskPopup = document.getElementById('custom-task-popup');
    taskPopup.style.display = 'none';
    document.body.classList.remove('blurred', 'no-scroll');
  }

  attachEventListeners() {
    document.getElementById('create-task-btn').addEventListener('click', () => this.openPopup());
    document.querySelectorAll('.delete-task').forEach((button) => {
      button.addEventListener('click', (event) => this.handleTaskDelete(event));
    });

    document.querySelectorAll('.move-task').forEach((button) => {
      button.addEventListener('click', (event) => this.handleTaskMove(event));
    });

    document.querySelectorAll('.clickable-task').forEach((taskElement) => {
      taskElement.addEventListener('click', (event) => {
        const taskId = event.target.closest('.task-info').dataset.id;
        const task = this.tasks.find((t) => t.id === taskId);
        this.openPopup(task);
      });
    });

    document.querySelector('.custom-close-popup').addEventListener('click', () => this.closePopup());

    window.addEventListener('click', (event) => {
      const taskPopup = document.getElementById('custom-task-popup');
      if (event.target === taskPopup) {
        this.closePopup();
      }
    });

    document.getElementById('custom-task-form').addEventListener('submit', (event) => this.handleTaskSubmit(event));
  }

  render() {
    const tasksHTML = this.tasks ? viewTasks(this.tasks) : viewTasks([]);
    this.el.innerHTML = tasksHTML;
    this.attachEventListeners();
  }

  run() {
    this.fetchTasks();
    setupHeaderEventListeners();
  }
};

export default Task;
