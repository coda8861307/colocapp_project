import Error404 from './controllers/Error404';

const Router = class {
  constructor(routes = [], path = window.location.pathname) {
    this.path = path;
    this.routes = routes;
    this.params = !window.location.search ? {} : Object.fromEntries(
      window.location.search
        .split('?')[1]
        .split('&')
        .map((param) => param.split('='))
    );

    this.run();
  }

  startController() {
    let ifExist = false;
    console.log(this.path);

    for (let i = 0; i < this.routes.length; i += 1) {
      const route = this.routes[i];

      if (route.url === this.path) {
        const Controller = route.controller;

        new Controller(this.params);
        ifExist = true;
        break;
      }
    }

    if (!ifExist) {
      new Error404();
    }
  }

  run() {
    this.startController();
  }
};

export default Router;
