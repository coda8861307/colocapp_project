<?php

namespace Back\Src\Controllers;

use Back\Src\Models\TaskModel;

class TaskController {
    private $model;

    public function __construct($pdo) {
        $this->model = new TaskModel($pdo);
    }

    public function getAllTasks() {
        $tasks = $this->model->getAllTasks();
        echo json_encode($tasks);
    }

    public function getTaskById($id) {
        $task = $this->model->getTaskById($id);
        echo json_encode($task);
    }

    public function createTask() {
        $data = json_decode(file_get_contents('php://input'), true);
        $name = $data['name'] ?? '';
        $description = $data['description'] ?? '';
        $date = $data['date'] ?? '';
        $author = $data['author'] ?? '';

        if ($name && $description && $date && $author) {
            $taskId = $this->model->createTask($name, $description, $date, $author);
            $task = $this->model->getTaskById($taskId);
            echo json_encode($task);
        } else {
            http_response_code(400);
            echo json_encode(['message' => 'Invalid input']);
        }
    }

    public function updateTask($id) {
        $data = json_decode(file_get_contents('php://input'), true);
        $name = $data['name'] ?? '';
        $description = $data['description'] ?? '';
        $date = $data['date'] ?? '';
        $author = $data['author'] ?? '';
        $completed = $data['completed'] ?? 0;

        if ($name && $description && $date && $author) {
            if ($this->model->updateTask($id, $name, $description, $date, $author, $completed)) {
                $task = $this->model->getTaskById($id);
                echo json_encode($task);
            } else {
                http_response_code(400);
                echo json_encode(['message' => 'Task update failed']);
            }
        } else {
            http_response_code(400);
            echo json_encode(['message' => 'Invalid input']);
        }
    }

    public function deleteTask($id) {
        if ($this->model->deleteTask($id)) {
            echo json_encode(['message' => 'Task deleted successfully']);
        } else {
            http_response_code(400);
            echo json_encode(['message' => 'Task deletion failed']);
        }
    }
}

