export const toggleNotificationMenu = (event) => {
  event.preventDefault();
  const notificationMenu = document.querySelector('.notification-menu');
  if (notificationMenu) {
    notificationMenu.style.display = notificationMenu.style.display === 'block' ? 'none' : 'block';
  }
};

export const closeNotificationMenu = () => {
  const notificationMenu = document.querySelector('.notification-menu');
  if (notificationMenu) {
    notificationMenu.style.display = 'none';
  }
};

export const toggleHamburgerMenu = (event) => {
  event.preventDefault();
  const dropdownContent = document.querySelector('.header-hamburger-container');
  const overlayblur = document.querySelector('body');

  if (dropdownContent && overlayblur) {
    if (dropdownContent.classList.contains('show')) {
      dropdownContent.classList.remove('show');
      overlayblur.classList.remove('blurred');
      overlayblur.classList.remove('no-scroll');
    } else {
      dropdownContent.classList.add('show');
      overlayblur.classList.add('blurred');
      overlayblur.classList.add('no-scroll');
    }
  }
};

export const closeHamburgerMenu = () => {
  const dropdownContent = document.querySelector('.header-hamburger-container');
  const overlayblur = document.querySelector('body');

  if (dropdownContent && overlayblur) {
    dropdownContent.classList.remove('show');
    overlayblur.classList.remove('blurred');
    overlayblur.classList.remove('no-scroll');
  }
};
