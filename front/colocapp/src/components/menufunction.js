// ../components/menufunction.js
export function toggleNotificationMenu(event) {
  event.preventDefault();
  const notificationMenu = document.querySelector('.notification-menu');
  notificationMenu.style.display = (notificationMenu.style.display === 'block') ? 'none' : 'block';
}

export function toggleHamburgerMenu(event) {
  event.preventDefault();
  const hamburgerMenu = document.querySelector('.header-hamburger-container');
  hamburgerMenu.classList.toggle('show');
  document.body.classList.toggle('blurred', hamburgerMenu.classList.contains('show'));
  document.body.classList.toggle('no-scroll', hamburgerMenu.classList.contains('show'));
}

export function closeHamburgerMenu() {
  const hamburgerMenu = document.querySelector('.header-hamburger-container');
  hamburgerMenu.classList.remove('show');
  document.body.classList.remove('blurred');
  document.body.classList.remove('no-scroll');
}
