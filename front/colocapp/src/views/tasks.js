import viewHeader from './header';
import viewFooter from './footer';
import viewTask from './task';

const viewTasks = (tasks) => (

  `
    ${viewHeader()}
    <main class="main-content">
        <section class="section">
            <div class="calendar-info-title">
                <p>Ici, tu peux voir toutes les tâches que ton/tes colocs ont uploadées !</p>
            </div>
            <div class="task-section-container">
                <div class="task-column">
                    <div class="header task">
                        <h3>Tâches non terminées</h3>
                        <svg id="create-task-btn" class="add-task" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                            <path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M12 4.5v15m7.5-7.5h-15" />
                        </svg>
                    </div>
                    <div class="task-container" id="incomplete-tasks">
                        ${tasks && tasks.length > 0 ? tasks.filter((task) => !task.completed).map((task) => viewTask(task)).join('') : ''}
                        <div id="custom-task-popup" class="custom-task-popup">
                            <div class="custom-task-popup-content">
                                <span class="custom-close-popup">&times;</span>
                                <h2>Modifier la Tâche</h2>
                                <form id="custom-task-form">
                                    <div class="custom-form-group">
                                        <label for="custom-task-name">Nom de la Tâche:</label>
                                        <input type="text" id="custom-task-name" name="task-name" required>
                                    </div>
                                    <div class="custom-form-group">
                                        <label for="custom-task-description">Description:</label>
                                        <textarea id="custom-task-description" name="task-description" rows="4" required></textarea>
                                    </div>
                                    <div class="custom-form-group">
                                        <label for="custom-task-date">Date Limite:</label>
                                        <input type="date" id="custom-task-date" name="task-date" required>
                                    </div>
                                    <div class="custom-form-group">
                                        <label for="custom-task-author">Auteur:</label>
                                        <input type="text" id="custom-task-author" name="task-author" required>
                                    </div>
                                    <button type="submit" class="custom-save-task">Enregistrer</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="task-column" id="completed-tasks">
                    <div class="header task">
                        <h3>Tâches terminées</h3>
                    </div>
                    <div class="task-container">
                        ${tasks && tasks.length > 0 ? tasks.filter((task) => task.completed).map((task) => viewTask(task)).join('') : ''}
                    </div>
                </div>
            </div>
        </section>
    </main>
    ${viewFooter()}
  `
);

export default viewTasks;
