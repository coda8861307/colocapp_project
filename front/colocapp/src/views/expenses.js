import viewHeader from './header';
import viewFooter from './footer';

export default () => (
  `
    ${viewHeader()}
    <main class="main-content">
        <section>
            <div class="expenses-submenu">
                <div class="calendar-submenu-info">
                    <div class="task-title-container expense">
                        <div class="icon expense-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 24 24">
                                <path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="m2.25 12l8.954-8.955a1.126 1.126 0 0 1 1.591 0L21.75 12M4.5 9.75v10.125c0 .621.504 1.125 1.125 1.125H9.75v-4.875c0-.621.504-1.125 1.125-1.125h2.25c.621 0 1.125.504 1.125 1.125V21h4.125c.621 0 1.125-.504 1.125-1.125V9.75M8.25 21h8.25" />
                            </svg>
                        </div>
                        <p>Loyer</p>
                    </div>
                    <div id="unfinishedTaskCount" class="amount">800 &euro;</div>
                    <div class="submenu-voirPlus">
                        <a href="">
                            <span>Date prélèvement : 15/06/24</span>
                        </a>
                    </div>
                </div>
                <div class="calendar-submenu-info">
                    <div class="task-title-container expense">
                        <div class="icon expense-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 24 24">
                                <path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="m3.75 13.5l10.5-11.25L12 10.5h8.25L9.75 21.75L12 13.5H3.75Z" />
                            </svg>
                        </div>
                        <p>Factures</p>
                    </div>
                    <div id="unfinishedTaskCount" class="amount">200 &euro;</div>
                    <div class="submenu-voirPlus">
                        <a href="">
                            <span>Date prélèvement : 15/06/24</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="expense-section-container">
                <div class="header expense">
                    <h3>Gestion des Dépenses</h3>
                    <svg id="add-expense-btn" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24">
                        <path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M12 4.5v15m7.5-7.5h-15" />
                    </svg>
                </div>
                <div class="expense-container" id="expense-list">
                    <!-- Les dépenses seront affichées ici -->
                </div>
            </div>
            <div id="custom-expense-popup" class="custom-popup">
                <div class="custom-popup-content">
                    <span class="custom-close-popup">&times;</span>
                    <h2>Ajouter une Nouvelle Dépense</h2>
                    <form id="custom-expense-form">
                        <div class="custom-form-group">
                            <label for="expense-description">Description:</label>
                            <input type="text" id="expense-description" name="description" required>
                        </div>
                        <div class="custom-form-group">
                            <label for="expense-amount">Montant:</label>
                            <input type="number" id="expense-amount" name="amount" required>
                        </div>
                        <div class="custom-form-group">
                            <label for="expense-date">Date:</label>
                            <input type="date" id="expense-date" name="date" required>
                        </div>
                        <div class="custom-form-group">
                            <label for="expense-category">Catégorie:</label>
                            <input type="text" id="expense-category" name="category" required>
                        </div>
                        <div class="custom-form-group">
                            <label for="expense-created-by">Créé par:</label>
                            <input type="text" id="expense-created-by" name="created_by" required>
                        </div>
                        <button type="submit" class="custom-save-expense">Enregistrer</button>
                    </form>
                </div>
            </div>
        </section>
    </main>
    ${viewFooter()}
  `
);
