import viewHeader from './header';
import viewFooter from './footer';

export default () => (
  `
  ${viewHeader()}
  <main class="main-content">
    <section class="section">
      <div class="calendar-info-title">
        <p>Bonjour NOM, voici le calendrier</p>
      </div>
      <div class="calendar-section-container">
        <div class="calendar-container">
          <div class="calendar-title">
            <h3>Calendrier</h3>
          </div>
          <div id="calendar"></div>
        </div>
        <div class="calendar-submenu">
          <div class="calendar-submenu-info">
            <div class="task-title-container task">
              <div class="icon task-icon">
                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24">
                  <g fill="none" stroke="currentColor" stroke-width="1.5">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M11.692 7.889h4.52M11.692 12h4.52m-4.52 4.111h4.52M8.066 8.506a.617.617 0 1 0 0-1.234a.617.617 0 0 0 0 1.234m0 4.111a.617.617 0 1 0 0-1.234a.617.617 0 0 0 0 1.234m0 4.111a.617.617 0 1 0 0-1.234a.617.617 0 0 0 0 1.234" />
                    <rect width="18.5" height="18.5" x="2.75" y="2.75" rx="6" />
                  </g>
                </svg>
              </div>
              <p>Tâches</p>
            </div>
            <div id="unfinishedTaskCount" class="count">0</div>
            <div class="submenu-voirPlus">
              <a href="?page=task">
                <span>Voir plus</span>
                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 24 24">
                  <g fill="none" stroke="currentColor" stroke-linecap="round" stroke-width="1.5">
                    <path stroke-miterlimit="10" d="M17.542 12H6.458" />
                    <path stroke-linejoin="round" d="m12.697 17.278l4.58-4.528a1.058 1.058 0 0 0 0-1.5l-4.58-4.528" />
                    <path stroke-linejoin="round" d="M12 21.5a9.5 9.5 0 1 0 0-19a9.5 9.5 0 0 0 0 19" />
                  </g>
                </svg>
              </a>
            </div>
          </div>
          <div class="calendar-submenu-info">
            <div class="task-title-container expense">
              <div class="icon expense-icon">
                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24">
                  <g fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5">
                    <path d="M12 21.5a9.5 9.5 0 1 0 0-19a9.5 9.5 0 0 0 0 19" />
                    <path d="M9 14.433a2.82 2.82 0 0 0 3 2.57c2.42 0 3-1.39 3-2.57s-1-2.43-3-2.43s-3-.79-3-2.4a2.75 2.75 0 0 1 3-2.6a2.89 2.89 0 0 1 3 2.6M12 18.5v-1.3m0-11.7v1.499" />
                  </g>
                </svg>
              </div>
              <p>Dépenses</p>
            </div>
            <div id="unfinishedTaskCount" class="count">0</div>
            <div class="submenu-voirPlus">
              <a href="?page=task">
                <span>Voir plus</span>
                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 24 24">
                  <g fill="none" stroke="currentColor" stroke-linecap="round" stroke-width="1.5">
                    <path stroke-miterlimit="10" d="M17.542 12H6.458" />
                    <path stroke-linejoin="round" d="m12.697 17.278l4.58-4.528a1.058 1.058 0 0 0 0-1.5l-4.58-4.528" />
                    <path stroke-linejoin="round" d="M12 21.5a9.5 9.5 0 1 0 0-19a9.5 9.5 0 0 0 0 19" />
                  </g>
                </svg>
              </a>
            </div>
          </div>
          <div class="calendar-submenu-info">
            <div class="task-title-container misc">
              <div class="icon misc-icon">
                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24">
                  <g fill="none" stroke="currentColor" stroke-width="1.5">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M11.692 7.889h4.52M11.692 12h4.52m-4.52 4.111h4.52M8.066 8.506a.617.617 0 1 0 0-1.234a.617.617 0 0 0 0 1.234m0 4.111a.617.617 0 1 0 0-1.234a.617.617 0 0 0 0 1.234m0 4.111a.617.617 0 1 0 0-1.234a.617.617 0 0 0 0 1.234" />
                    <rect width="18.5" height="18.5" x="2.75" y="2.75" rx="6" />
                  </g>
                </svg>
              </div>
              <p>Misc</p>
            </div>
            <div id="unfinishedTaskCount" class="count">0</div>
            <div class="submenu-voirPlus">
              <a href="?page=task">
                <span>Voir plus</span>
                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 24 24">
                  <g fill="none" stroke="currentColor" stroke-linecap="round" stroke-width="1.5">
                    <path stroke-miterlimit="10" d="M17.542 12H6.458" />
                    <path stroke-linejoin="round" d="m12.697 17.278l4.58-4.528a1.058 1.058 0 0 0 0-1.5l-4.58-4.528" />
                    <path stroke-linejoin="round" d="M12 21.5a9.5 9.5 0 1 0 0-19a9.5 9.5 0 0 0 0 19" />
                  </g>
                </svg>
              </a>
            </div>
          </div>
          <div class="calendar-submenu-info">
            <div class="task-title-container misc2">
              <div class="icon misc2-icon">
                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24">
                  <g fill="none" stroke="currentColor" stroke-width="1.5">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M11.692 7.889h4.52M11.692 12h4.52m-4.52 4.111h4.52M8.066 8.506a.617.617 0 1 0 0-1.234a.617.617 0 0 0 0 1.234m0 4.111a.617.617 0 1 0 0-1.234a.617.617 0 0 0 0 1.234m0 4.111a.617.617 0 1 0 0-1.234a.617.617 0 0 0 0 1.234" />
                    <rect width="18.5" height="18.5" x="2.75" y="2.75" rx="6" />
                  </g>
                </svg>
              </div>
              <p>Misc2</p>
            </div>
            <div id="unfinishedTaskCount" class="count">0</div>
            <div class="submenu-voirPlus">
              <a href="?page=task">
                <span>Voir plus</span>
                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 24 24">
                  <g fill="none" stroke="currentColor" stroke-linecap="round" stroke-width="1.5">
                    <path stroke-miterlimit="10" d="M17.542 12H6.458" />
                    <path stroke-linejoin="round" d="m12.697 17.278l4.58-4.528a1.058 1.058 0 0 0 0-1.5l-4.58-4.528" />
                    <path stroke-linejoin="round" d="M12 21.5a9.5 9.5 0 1 0 0-19a9.5 9.5 0 0 0 0 19" />
                  </g>
                </svg>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  ${viewFooter()}
  `
);
