# ColocApp

ColocApp is a web application designed to facilitate task and expense management within a shared household, enabling roommates to stay organized and equitably share domestic responsibilities and finances. It's for a final project for my first year in CODA, as a Junior FullStack Developper. I didn't finished properly the back and Front, and so besides showing pages, this project doesn't do much else. I'll finish it even after the due date, because I like to finish my project and I enjoyed coding this website.

## Features

- **Task Management:**
  - Create, assign, and track household tasks. (Back doesn't work for now)
  - Set priority, due dates, and categories for each task. (Back doesn't work for now)
  - Mark tasks as completed with progress tracking. (Back doesn't work for now)

- **Expense Management:**
  - Enter and track shared expenses such as rent, bills, and groceries. (Back doesn't work for now)
  - Equitably distribute expenses among roommates. (Back doesn't work for now)
  - Track payments and receive reminders for overdue expenses. (Back doesn't work for now)

- **Shared Calendar:**
  - View upcoming tasks and important events in a shared calendar. (Back doesn't work for now)

- **Personal Dashboard:**
  - Personalized display of assigned tasks, pending expenses, and important notifications for each roommate. (Back doesn't work for now)

## Technologies Used

- **Frontend:** HTML, CSS, JavaScript
- **Backend:** PHP, MySQL
- **Architecture:** Model-View-Controller (MVC), and a Node.js like architecture.
- **Required Dependencies:** SweetAlert2, Axios

## Installing Dependencies

To install the necessary dependencies for the application in the front directory:

```bash
# Install SweetAlert2
npm install sweetalert2

# Install Axios
npm install axios

```

It might have already the necessary dependecies, because this repo is a clone of two repos : one for the back, the other for the front, and I had trouble making them into gitlab.

**OH and I don't have access to my database and so I couldn't move it here in time... had a lot of bugs happening at once just before sending this...**


## Database Documentation - ColocApp

This document outlines the structure of the database tables used by ColocApp, because I couldn't move it in this project

### Tables

#### Table `users`

This table stores information about registered users in the application.

- **Attributes:**
  - `id` (int): Unique identifier for the user
  - `username` (varchar): User's username
  - `password` (varchar): Hashed password
  - `email` (varchar): User's email address
  - `firstName` (varchar): User's first name
  - `lastName` (varchar): User's last name
  - `created_at` (timestamp): Date and time when the user account was created

#### Table `expenses`

This table records shared expenses among roommates.

- **Attributes:**
  - `id` (int): Unique identifier for the expense
  - `title` (varchar): Title of the expense
  - `category` (varchar): Expense category
  - `dueDate` (date): Due date for the expense
  - `description` (text): Detailed description of the expense
  - `amount` (decimal): Amount of the expense
  - `status` (enum): Status of the expense (pending, paid, etc.)
  - `created_by` (int): ID of the user who created the expense
  - `created_at` (timestamp): Date and time when the expense was created

#### Table `tasks`

This table manages tasks assigned to roommates.

- **Attributes:**
  - `id` (int): Unique identifier for the task
  - `title` (varchar): Title of the task
  - `description` (text): Detailed description of the task
  - `dueDate` (date): Due date for the task
  - `status` (enum): Status of the task (in progress, completed, etc.)
  - `created_by` (int): ID of the user who created the task
  - `forwho` (int): ID of the user assigned to the task
  - `created_at` (timestamp): Date and time when the task was created

#### Table `event`

This table stores important events for roommates.

- **Attributes:**
  - `id` (int): Unique identifier for the event
  - `title` (varchar): Title of the event
  - `description` (text): Detailed description of the event
  - `date` (date): Date of the event
  - `created_by` (int): ID of the user who created the event
  - `forwho` (int): ID of the user associated with the event
  - `created_at` (timestamp): Date and time when the event was created

#### Table `shared_expenses`

This table associates users with shared expenses.

- **Attributes:**
  - `id` (int): Unique identifier for the association
  - `user_id` (int): ID of the user associated with the shared expense
  - `expenses_id` (int): ID of the shared expense associated
  - `amount` (decimal): Amount allocated to the user for the shared expense
  - `status` (varchar): Contribution status (paid, pending, etc.)


### Im sorry for sending this so unfinished project which worked in another environment but not in this version.
*You will soon find this version on my github.* 

**Thank you for your time, Goodbye.**
