import axios from 'axios';
import Swal from 'sweetalert2';
import viewLogin from '../views/login';
import setupHeaderEventListeners from '../components/eventListener';

const Login = class {
  constructor(params) {
    this.params = params;
    this.el = document.querySelector('#root');
    this.baseUrl = 'https://localhost:83/login';
    this.run();
  }

  async loginToBack(data) {
    try {
      const response = await axios.post(this.baseUrl, data);
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: response.data.message,
        showConfirmButton: false,
        timer: 1500
      });
      // Handle storing the session token or redirecting the user
      return response;
    } catch (error) {
      Swal.fire({
        icon: 'error',
        title: 'Erreur',
        text: error.response.data.message
      });
      return error;
    }
  }

  handleLoginSubmit(event) {
    event.preventDefault();

    const username = document.getElementById('username').value;
    const password = document.getElementById('password').value;

    if (!username || !password) {
      Swal.fire({
        icon: 'error',
        title: 'Erreur',
        text: 'Veuillez entrer votre nom d\'utilisateur et votre mot de passe.'
      });
      return;
    }

    const data = {
      username,
      password
    };
    this.loginToBack(data);
  }

  render() {
    return `
      ${viewLogin()}
    `;
  }

  run() {
    this.el.innerHTML = this.render();
    setupHeaderEventListeners();

    const form = document.getElementById('login-form');
    if (form) {
      form.addEventListener('submit', this.handleLoginSubmit.bind(this));
    }
  }
};

export default Login;
