import logo from '../images/colocapp_logo.png';
import account from '../images/account.png';

export default () => (
  `
  <header>
    <div class="header-menu">
      <a class="header-hamburger-button">
        <svg class="svg" xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 24 24">
          <path fill="currentColor" fill-rule="evenodd" d="M3 6.75A.75.75 0 0 1 3.75 6h16.5a.75.75 0 0 1 0 1.5H3.75A.75.75 0 0 1 3 6.75ZM3 12a.75.75 0 0 1 .75-.75h16.5a.75.75 0 0 1 0 1.5H3.75A.75.75 0 0 1 3 12Zm0 5.25a.75.75 0 0 1 .75-.75h16.5a.75.75 0 0 1 0 1.5H3.75a.75.75 0 0 1-.75-.75Z" clip-rule="evenodd"/>
        </svg>
      </a>
    </div>

    <div class="header">
      <div class="header-hamburger-container">
        <div class="header-hamburger-content">
          <div>
            <a class="header-hamburger-close">
              <svg class="svg" xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 32 32">
                <path fill="currentColor" d="M7.219 5.781L5.78 7.22L14.563 16L5.78 24.781l1.44 1.439L16 17.437l8.781 8.782l1.438-1.438L17.437 16l8.782-8.781L24.78 5.78L16 14.563z"/>
              </svg>
            </a>
          </div>
          <a class="header-hamburger-a" href="/calendar">Calendar</a>
          <a class="header-hamburger-a" href="/tasks">Tasks</a>
          <a class="header-hamburger-a" href="/expenses">Expenses</a>
          <a class="header-hamburger-a" href="/dashboard">Dashboard</a>
        </div>
      </div>

      <div class="header-logo-container">
        <a href="?page=home">
          <img class="header-logo" src="${logo}" alt="logo"/>
        </a>
      </div>

      <div class="header-navigation">
        <nav class="header-navbar">
          <div class="header-link-container">
            <a href="/calendar">Calendar</a>
          </div>
          <div class="header-link-container">
            <a href="/tasks">Tasks</a>
          </div>
          <div class="header-link-container">
            <a href="/expenses">Expenses</a>
          </div>
          <div class="header-link-container">
            <a href="/dashboard">Dashboard</a>
          </div>
        </nav>

        <div class="header-navbar-2">
          <div class="svg-container">
            <a class="header-notif-button">
              <svg class="header-notif" xmlns="http://www.w3.org/2000/svg" width="35" height="35" viewBox="0 0 24 24">
                <g fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5">
                  <path d="M11.962 17.986h6.81a1.555 1.555 0 0 0 1.512-2.175c-.36-1.088-1.795-2.393-1.795-3.677c0-2.85 0-3.6-1.404-5.276a5.025 5.025 0 0 0-1.653-1.283l-.783-.38a1.089 1.089 0 0 1-.511-.73a2.023 2.023 0 0 0-2.176-1.707a2.023 2.023 0 0 0-2.12 1.707a1.089 1.089 0 0 1-.567.73l-.783.38A5.025 5.025 0 0 0 6.84 6.858c-1.403 1.676-1.403 2.426-1.403 5.276c0 1.284-1.37 2.458-1.73 3.611c-.217.697-.337 2.241 1.48 2.241z"/>
                  <path d="M15.225 17.986a3.198 3.198 0 0 1-3.263 3.263A3.195 3.195 0 0 1 8.7 17.986"/>
                </g>
              </svg>
              <div class="notification-badge">1</div>
            </a>
            <div class="notification-menu" style="display:none;">
              <div class="notification-item">
                <a href="/login">
                  <p>Login</p>
                </a>
              </div>
              <div class="notification-item">
                <a href="/register">
                  <p>Register</p>
                </a>
              </div>
              <!-- Ajoutez plus d'éléments de notification ici -->
            </div>
          </div>
          <a href="/login">
            <img class="accountimage" src="${(account)}" alt="account.png"/>
          </a>
          <div class="header-task-button">
            <a href="/tasks">
              <svg class="header-task-button-alternate" xmlns="http://www.w3.org/2000/svg" width="23" height="23" viewBox="0 0 24 24">
                <path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M12 4.5v15m7.5-7.5h-15"/>
              </svg>
              <div>New Task</div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </header>
  `
);
