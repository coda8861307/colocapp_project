import Router from './Routeur';
// import ColocApp from './controllers/ColocApp';
import Login from './controllers/Login';
import Register from './controllers/Register';
import Calendar from './controllers/Calendar';
import Task from './controllers/Task';
import Expense from './controllers/Expense';
import Profile from './controllers/Profile';

import './index.scss';

const routes = [
  {
    url: '/homepage',
    controller: Calendar
  },
  {
    url: '/login',
    controller: Login
  },
  {
    url: '/register',
    controller: Register
  },
  {
    url: '/calendar',
    controller: Calendar
  },
  {
    url: '/tasks',
    controller: Task
  },
  {
    url: '/expenses',
    controller: Expense
  },
  {
    url: '/profile',
    controller: Profile
  }
];

new Router(routes);
