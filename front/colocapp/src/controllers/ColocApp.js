import viewCalendar from '../views/calendar';
import setupHeaderEventListeners from '../components/eventListener';

const ColocApp = class {
  constructor() {
    this.el = document.querySelector('#root');
    this.run();
  }

  render() {
    return `
    ${viewCalendar()}
    `;
  }

  run() {
    this.el.innerHTML = this.render();
    setupHeaderEventListeners(); // Appel de la fonction pour configurer les écouteurs d'événements
  }
};

export default ColocApp;
