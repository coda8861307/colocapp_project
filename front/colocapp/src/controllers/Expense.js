import viewExpense from '../views/expenses';
import setupEventListeners from '../components/eventListener';

const ColocApp = class {
  constructor() {
    this.el = document.querySelector('#root');
    this.run();
    setupEventListeners(); // Appel de la fonction pour configurer les écouteurs d'événements
  }

  render() {
    return `
    ${viewExpense()}
    `;
  }

  run() {
    this.el.innerHTML = this.render();
  }
};

export default ColocApp;
