<?php

namespace Back\Src\Models;

use PDO;
use PDOException;

// Headers CORS
header("Access-Control-Allow-Origin: http://127.0.0.1:9090");
header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
header("Access-Control-Allow-Credentials: true");

// Si la méthode est OPTIONS, renvoyer une réponse 200
if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    http_response_code(200);
    exit();
}

class UserModel extends SqlConnect {
    private const TABLE = 'users';

    public function add(array $data): array {
        try {
            $query = "
                INSERT INTO " . self::TABLE . " (username, first_name, last_name, email, password, created_at)
                VALUES (:username, :first_name, :last_name, :email, :password, :created_at)
            ";

            $existingUser = $this->getUserByEmail($data['email']);
            if ($existingUser) {
                return ['status' => false, 'message' => 'Adresse email déjà utilisée.'];
            }

            $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
            $data['created_at'] = date('Y-m-d H:i:s');

            $stmt = $this->db->prepare($query);
            if ($stmt->execute($this->transformDataInDot($data))) {
                return ['status' => true, 'message' => 'Votre compte a été créé avec succès'];
            }
            return ['status' => false, 'message' => 'Une erreur est survenue, veuillez réessayer.'];
        } catch (PDOException $e) {
            return ['status' => false, 'message' => 'Erreur de base de données : ' . $e->getMessage()];
        }
    }

    public function login(string $username, string $password): array {
        try {
            $stmt = $this->db->prepare("SELECT * FROM " . self::TABLE . " WHERE username = :username");
            $stmt->execute([':username' => $username]);
            $user = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($user && password_verify($password, $user['password'])) {
                return ['status' => true, 'message' => 'Connexion réussie!', 'user' => $user];
            }

            return ['status' => false, 'message' => 'Nom d\'utilisateur ou mot de passe invalide.'];
        } catch (PDOException $e) {
            return ['status' => false, 'message' => 'Erreur de base de données : ' . $e->getMessage()];
        }
    }

    public function setSessionToken(int $userId, string $token): array {
        try {
            $stmt = $this->db->prepare("UPDATE " . self::TABLE . " SET session_token = :token WHERE id = :id");
            if ($stmt->execute([':token' => $token, ':id' => $userId])) {
                return ['status' => true, 'message' => 'Jeton de session défini avec succès.'];
            }
            return ['status' => false, 'message' => 'Échec de définition du jeton de session.'];
        } catch (PDOException $e) {
            return ['status' => false, 'message' => 'Erreur de base de données : ' . $e->getMessage()];
        }
    }

    public function getUserByEmail(string $email) {
        try {
            $stmt = $this->db->prepare("SELECT * FROM " . self::TABLE . " WHERE email = :email");
            $stmt->execute([':email' => $email]);
            return $stmt->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            return null;
        }
    }
}
