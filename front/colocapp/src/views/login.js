import viewHeader from './header';
import viewFooter from './footer';

export default () => (
  `
  ${viewHeader()}
  <main class="main-content">
    <section>
      <div class="login-container">
        <div class="login-form">
          <h2>Connexion</h2>
          <form action="/login" method="POST">
              <div class="form-group">
                  <label for="username">Nom d'utilisateur</label>
                  <input type="text" id="username" name="username" required>
              </div>
              <div class="form-group">
                  <label for="password">Mot de passe</label>
                  <input type="password" id="password" name="password" required>
              </div>
              <button type="submit">Se connecter</button>
              <a href="?page=register">S'inscrire</a>
          </form>
        </div>
      </div>
    </section>
  </main>
  ${viewFooter()}
`
);
