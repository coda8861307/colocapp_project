import axios from 'axios';
import Swal from 'sweetalert2';
import viewRegister from '../views/register';
import setupHeaderEventListeners from '../components/eventListener';

const Register = class {
  constructor(params) {
    this.params = params;
    this.el = document.querySelector('#root');
    this.baseUrl = 'http://localhost:83';
    this.run();
  }

  async registerToBack(data) {
    try {
      const response = await axios.post(`${this.baseUrl}/register`, data);
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: response.data.message,
        showConfirmButton: false,
        timer: 1500
      });
      return response;
    } catch (error) {
      Swal.fire({
        icon: 'error',
        title: 'Erreur',
        text: error.response?.data?.message || 'Une erreur s\'est produite'
      });
      return error;
    }
  }

  handleRegisterSubmit(event) {
    event.preventDefault();

    const username = document.getElementById('username').value;
    const firstName = document.getElementById('first-name').value;
    const lastName = document.getElementById('last-name').value;
    const email = document.getElementById('email').value;
    const password = document.getElementById('password').value;
    const confirmPassword = document.getElementById('confirm-password').value;

    if (!username || !firstName || !lastName || !email || !password || !confirmPassword) {
      Swal.fire({
        icon: 'error',
        title: 'Erreur',
        text: 'Veuillez compléter tous les champs.'
      });
      return;
    }

    if (password !== confirmPassword) {
      Swal.fire({
        icon: 'error',
        title: 'Erreur',
        text: 'Les mots de passe ne correspondent pas.'
      });
      return;
    }

    if (!this.validateEmail(email)) {
      Swal.fire({
        icon: 'error',
        title: 'Erreur',
        text: 'Veuillez entrer une adresse email valide.'
      });
      return;
    }

    const data = {
      username,
      firstName,
      lastName,
      email,
      password
    };
    this.registerToBack(data);
  }

  validateEmail(email) {
    const re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return re.test(email);
  }

  render() {
    return `
      ${viewRegister()}
    `;
  }

  run() {
    this.el.innerHTML = this.render();
    setupHeaderEventListeners();

    const form = document.getElementById('register-form');
    if (form) {
      form.addEventListener('submit', this.handleRegisterSubmit.bind(this));
    }
  }
};

export default Register;
